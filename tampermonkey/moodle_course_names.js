// ==UserScript==
// @name         moodle_course_names
// @version      1.3.7
// @description  Displays courses' names instead of random numbers!
// @author       Valerio
// @match        https://moodle.novasbe.pt/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=novasbe.pt
// @updateURL    https://gitlab.com/onpower0/valerio/-/raw/main/tampermonkey/moodle_course_names.js
// @downloadURL  https://gitlab.com/onpower0/valerio/-/raw/main/tampermonkey/moodle_course_names.js
// @grant        GM.setValue
// @grant        GM.getValue
// @run-at       document-end
//
// ==/UserScript==

(function () {
    'use strict';

    // Your code here...
    let a1 = document.getElementsByClassName("logo d-none d-sm-inline")[0].children[0];
    a1.src = "https://i.imgur.com/noPFYgD.png";
    let img = document.createElement('img');
    img.src = "https://i.imgur.com/NmVRDuR.png"
    img.width = "70";


    document.getElementsByClassName("logo d-none d-sm-inline")[0].appendChild(img);

    let navdrawer = document.getElementById("nav-drawer");

    navdrawer.style.backgroundImage = "url('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUVFRgWEhUYGBIYFRUYGBkYGBgYGBgYGRgZGRgYGBgcIS4lHB4rHxgYJjgmKy8xNTU1GiU7QDs0Py40NTEBDAwMEA8QHxISHjQkJCs0NDQxNDQ0NDQ0NDExNDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDE0NDQ0NDQ0NDQ0NDQ0NDQ0NP/AABEIAT4AnwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAADAQIEBQYABwj/xABBEAACAQIDBAgDBQYGAQUAAAABAgADEQQSIQUxQVEGEyJhcYGRoTKxwVJictHwM0KSorLhBxQjY4LCcxUWJOLx/8QAGQEAAwEBAQAAAAAAAAAAAAAAAQIDAAQF/8QAIxEAAgIDAQEAAwADAQAAAAAAAAECEQMhMRJBEzJRIiOBBP/aAAwDAQACEQMRAD8A9aUR/VxUEMBEUbGbI/VRCklWjWEPlAsjBYmIo3W0Ko1hSNIPKaDdGfrUbGScNhriExSayXhF0kIwXopKWgIwcj4rDWEt7SLjRpKygqEjJ2ZOuTeHwSXMTEJrJmz01nLFboqyxp4PSK+Dk+mNI8idf41RL0yBRp5RaFCXj2EegmUVwzYBqdo0LJVQQaCHyCwDUYN8NmGssQsXLN4TNYJYYQSwohQGLGNHRrQsAxYQwawhgQWQMQNZIw+6R6++Homwudwk4/sPLhJkXGbpKkTFnSUlwWPTP1l1kvADWR6w1knBnUDid05I/sWlwvE3RxgqDXGtr8bd2n0hTOxcIAmj0jDHrFXQvh1SMSPqRiQvpvgYTpwnRhQSwogVhCYqCx0a0QPFaGzDFjyYK8DVxFjaL6SQfNg8QdYLaFbLQezhGyNlY6hWIsrEcQDaRNoY1UVnchUUFmJNgFAuTfu3zF9Iel5y/wDxgjoVZg4e5zZbISm9bE5rb+yB3yUXbdFHHQlf/E1kGSthnSqAAy6ZQbb1ZjqOV1PDfKQdNlBLrQdqh41KpZbcyihQfCw8ZkMbiWfWo7O2+xNwL/0+Ejmpca/2lfKfQW1xG1p9L1NReswwp027TPTdqRt9sZN/gc14PbHTIh0bB4liyE5RUpFQytlzIzXGZTlG9R47jMqlXN2G1VrKLasDfSxPnEbZt7im6OwIGUNY68idG/Wkm8auxkz3PobjamJqVsQRag60Upi/FAzPYb7Xfed81zGfPnQLb1TD4gYcMAtVii8clW3YuRvQsApX71xrPbMDtQVEV7WzDx4kaHlcGOmoqhHFt2WN4RZEo1LiHV4YsVofUjUjXeOpzXs3wNOjS0RXjWLR1rQNSpCVWkJ2iSdDRQ8VJJpveVzNJGHaLGWxmtEwpI74e8lCI0o4piptGK6YYIumVhekEd3BNgzIUCBvu3Yse9RwvPDqhsTY5gD4X5kHeL/oT3jpng3q0+rpuwaqwQDTKosWZrCxYhVYi5OoFp4FtCi9J8rDXKrjXejgFT5ggyUaTZX4BLMDa31jry22jsY06dJi92dVNRfslgGVRbuz631ymV60xew5E/SP6TM4taYJWPreEV/EHXWPanfdwB9ecCKbMbD4u4G/n3QqVgao7DVOrdXT4kYMCeBGoInvfQegy4LCo4s4oXI5AnMAfIj1nhiUupdesTNUGVlV7ZOYzqDdhuNrgHjpv9e/wvxlSs1UuzMVC5nNu3UezMwAAAUAqgA0sgtbdFasF0bmglriSVSBB7UlrDFfBZMH1cUC0LAVmjNJAWwNapBrWjakBeSbdjpFjXeQWMm4hZBaGXQRGMZJwxkSS8MIsejPhLeraMavFaneI1CO/XwRUQMRUX4msAtzc8NCCfQmfPXSJy7U2JJPVBAbE3CMwIvbQq+ZbfZCnjPb+lWMSjSfPlLFHKg66gaNbkCQSZ4TRJL6XsjZrXvY/ELX49kRYJ7bHb+F1h1rYlKdMhSUDDNfVxSz5RfiQpZb8d/OVFejkIN9WRW8AwzL/LY+c9U2Zs6nRdCqgIAbW3BWzgC/cCgnnm3sKUrVFtojKgt9lQFT+UCTUts6HH/FEfZOFD1EV79WxCtbfYDX5TWYnZ1HDDOCi08ouupqq+vbpN8QJJXQm28bt79ibLHU4epTsWAJb712a48dSPKTdo7GNYjtnfuNso78oHaPifOBydjKKSMPTwr4l6lWpc2F3YAcrIgtuJIA04X5T2j/AA/2T1GHuR23CBj/AONAmndmDnzlJhdiIiJTp3Iub3A+K3ac2Au7ajuGgsNJvlZVUKtgoAAA3ACWhJM5skWnRHqvZhJqtK2u1zJ9AdkTRe2JJaCF5GrPJBSRay2jysVUCaAaGgmEmx0WVeV1WWNeV1UzT6aIISbhpCWSlawA5xYjMsKcFjMQtNC7fCo4bydwUDiSSABxJEIhAFzoANZh9r9IFKf5tu1QVyuFQa9Y+Vh1xUntakZRwXXjpdcI/TH/AOJe1CHWmxBruodwLkIlyEpr3Xzknja9u0AMvsDFIlRutBFNkCkoBcMLAPrx3nxO6JtWlVaq9aupz1GzljqCugUI3FQoWx46ecXDJmcgC2oJtewB1NrwpJpIdNpnpmyKtkSm7K4VQqVAey6gWW4Oqva117r+ET/0XPjRdbqVDAHcSCot38dJmaW03osGpmzcRwI5MOM33RTadLE63K4hO0FsSLcTm77nQ8uNpHJhado6VlXmhmBwHUL1Y+FSct/iAJJs3eN1+IAlpgMGxIJO4ggjle9vHhJ+KqU2yqQC+oOpzW4XI+v0kjZ6jhukq3QJS1YZMKd9hv4CFamZYKsVllvx6OdzbKkIbyzw+6RKg1kyjugxqmaTtBTImIksyLiJWXBURIxxHxrCSHJ9eVlTfLHEmVzzT6aPBEEcj3buEC75ReJgzrJXuh61YDpnWf8Ay5w9EE18TmpJlvoChLsSPhWwC34ZhK/EbIw9ErUxLGpkRUo0BbIihQvw/vtpqx03aaCW21do9Vu1qEctFXu9POZWtiGcksbkykpt6QY49Wys6VOtanWcBs2RioNjay9kWGg3CYOmCjvYXWysLbsjhSvsQJ6VUoq3xCYPAYfJijRftAB6Yv8AZW7J7W9Y0H54aSK16lzeW/Rva3+XaqbsC9JVXKSDmWtTfeN3YV9ZF21s40jcfAePLuMi7CrJ/maQqANTL5XB3EMpXy1I1nVcZRsltSPWMNiKIXMylgRxICm+pN9COHvJFHaTCxViEBFr9pjc6KGYHs+UphWFOjnCkolK9hqTYG4txPZtJuGDOAyjOTZlIVhYEXuQd3mBvnnb+Ha0vprcNt1To65T43HrwlkmKVhdWBHMEEeo0mCBddHBvxFoSnicpuDY9xsZSOV/SMsC+GxqHWTKO6UWAxOcXO8Gx7uXkdfQ90vKG6PB2yM1Wg0i4iSSZGrykuCIiGc0Ro4SQ4XEPITSfiEldiGsIJ66GJExD3NuAheuyUsw+JmyjwH97yMBO2o9lpryTMfFiTILbsrS4U+MrG5PHWQcpGvGGranzjXGkslo1nK1xfhMl0iQUcVSr7kYrmPehAa/ihHpNPex7j8+B9pWdIcH12HdQLunbX/jvA8VLDzhj0EtosHoo6lHAZWFtbHfynne29kvhagYa08wZG8Dexmy6MVy+HRWN3RQL811yHysV8UMsto4FK9NkcaEb+IPAiPGVMDVoHsqsKqBMpIsGbkKbDMtiOFzv4XEtej+2KiYqtSqAKGYdWDawCiw7Q/dO/uu0peiqPS6tamhUvh3PAqfgYdxBWXu1dnaq4/aKeyeZXcD3MundaLSTY9+lTNjTpU31sCpNsp1CtfdbcNd4525zNbTwoR7qOy2o/Cdx9IbBYi/ZY9ksrG/do38pPpGbXxudrkZQNBrqRwPK2ptaK36XNixuMu6O2ZihTa7fCbcOW76jzmmo1v146zENUOptp38ZsNmOHReeUD0G/zhja0hclPZLevAirmkhqEr6nYaFuS6IknwI0VYpiKJjE2st5T4pbmXJaRq+HmyR9LRouioVJE21oVB35FHoTLxcP3Sg26b5G5hv62P1koxoqpFMd8Rt05ZzGVvRiNiNBc8CvreOQ8e/wCUbi0un/ND/MBFqOFUeHuYAlLstBSrMi6L1jpb7roK1PyH+oP+c0QmSx9fJVrG9iow1Typugb+VmE1pEyAc63FvTuI1B9ZepVFSmD9pQfA/wD7KGTtk1dHS+qtm8n1+eaEw4Pr6gg+hBhscMiKN6nK6E62BHbQnuJHtA4kWY94vB1HbqkJOgqMLfdbNu/ggoa+MFVe4HfrLvo7i7NkPl5f2lAd9obBVSjq3IzXQGrPRCZX42leOp4jQcrAjwOo/Lyhx2hHk1JUc6TiyBhm0seEkBYjUbNcSUiRYp8DJigQhEaohBKpCMZlmH2hj6VYXptdVZwTrobkEWOoOg0O6829Y2UkbwCR420nk20+j4oYpquGqEK2fPTJNrnedN4vmsGBtfuiTUefSmO+lp1iD94eHGBGplE9XEqxJUZAp+E2F9bZQe63neRsNtzFghRhs1zq3aBI7s1xpES/hZtGnxNE9Xc6XYfWV9QE6ncNw+pkhq1R6aNUspYG6A3KsCQQSd574C195vAEyPSFyargfvUsp8LZvoJr0xDWF+QmS2qmbElBxZE/iVQfS59JqmWFM1BzWMXZVcjFMODqy+liP6T6xlJdYBGy1qb/AHxfwY6+xMIGaLaRsFPeRKahhq1WstiFoUwxOa9mzA3y82104esvNoJdPBgfp9YoBRUUDeN/Di2vlA3QPhXulrd3HmDuI7oxWhh8NjrbcYG2sUY12za4akuuq3U943jX1lvhxM3sCp2WHGx+Rmmox49IzCFI8Tp0vREYsfGLHwILGuBY33W1mA2sQahIHDXx5zeYh8qk93vwmC2r8b/j/P8AtJ5Oori4ysqLxiLoRzisfzj8MmZ073UepER8KIXHoQfui4Hhcm/vISm00m2cNbKdwZF8uyDMXtXHlFbq1JYaZiCEUnQHMfiPct++2+TT3THe1aKbBt1mMvwFR29AxX2HuJrSJneimEszPwUZQTxY6k+nzmlAjRNLTOAsJExC6SW26NZL2XmR+Z9rxmxVs0ezKq16ZI3lbEcnAvb11kjFYUuqWDC6roNzCy3II4a2MpdmYg0qhIS6MoBtYWYHQ28C00myMctRRTcWcG6Hfqq6EcjYel4qak6NKMo76ihekWBBFnBse+1tRIrCaXG4Bs1wL87cTztw/WsqMVQ38DxG6A1krYJFzc2Fj7ggTW0Jk+j9PMzchlJ99JrKEpDpPIHEWIIsuQBKY8mCWc+6Knoail6X7R6nDswPaIa3iFJHvYecze131XvRX/jC/lE6e02r4jC4VTbrM7N3KGS59FaJtRe2CBZCqZAeChECyTlbLQVIr3haDDMvay6g3sTa2t7DWDdCT/eCaiBqbd8DQyNdjcSlSjdShtoDnAuRqAf0Zgtq4GpWYBsqqDeytm8ySBc2vYWtrvkyk1yeQ3ePPxiqnEmL5voyfngLDUQgCqMqqO4/owoct/bS8VzbQ8ZyG+7dCB7OZdbbz67u8wuFpnNqeBA+Z+XtGMmlxvBv+fteSqI1FuY1+ftA+Gi6ZLRJcbDUiqOzfskE/ZHP6ecg4ZdCTul7saiVYk6Zhu9CL+X61EEY20PklUWTnOsFi9n06o7VweY0MkVKcYUMptfDmIeFwKUQcpJJtck8t2g0EsMO8hVVMJh1MWL/AMgtWtlkGi5oJQYtpeyVHU4S0HThZo8MzO4nZ+baCViNKeEYD8TVD9LzNbSxAJp63K0Kam32gozD1noFQC45lWHynkWPxvU1XVhcZ3tzFmIksrUWrL4Yud18LLU79ByH5wFYa9rdwAi4bEo6Zy2RBoSwY2On2QftD1lvsbZa1wXSorKGKkqCxBABI7VgNCOMVO+FGnHpT5TyhEpHjNnT6PIBopJ5sQbeCjT3PhKbFkUy2RVZgRYtcjUA3AFue43haaWxVJN6KNsKSczXAO7vE4G3A2j3xDsxuRfw/Mwbknfr8vSLtjaH064vawHibnyBhWx1jawOmvDwkFxbdGAzUg2yyO0Hay6Ktx8N728bzc4I3AI5g/n7Xnm17TebDxYakrDWygEcbqNR+uceHSWS2jRZYhWOBiGWogQsSsLhlg8QYXDbpJfsO/1JFp1os6WJkam8LmleHjnq6SalQ7iGrVRoeR9jof13TyDpkmXENbdmb1zX/wCwnoFfFm++YjpPQ612KftFykrfVwyjVRxYFCCO6cuWfo7P/NHy2aHYuzFOECP+8aiseWcZQfIhfSB/w/q/5apVw9U2Z2JUE7qiXWoniVyOOam/CStnV/8ASUEaFBcHjca3HrKfaGzk69ajOyhsih9DkdfgDE77jQNvBA11gjOqaNNem4s9KqVAVIva4IvyuLXmax6DJU07RIN+Vm0UeQPvIOExGIDnrnV1HwZOwPFkIvm/5Ed0PtFioUHe5Zz4bkH9R85V5PSZGMPLKNk1gma/65SS41kPgfSMnoLWwLmNG+OaNp74BkFUXMuujddkfKQSjEbuFuPpp6SnTTU7v1oJbbIqdq9tLrp3XBPtNwDVm7p1xYX32AMU4kc5SbTqlH+6wBHyPv8AOQTjTzmeZx0TWO9l3icSJKwlUWmWOKuZcYKp2YITuVmlGkXXWCLnlcKseKkv6JeSHnnVKnZMExiOeyfCTKFFi6+synSE9tT9tCP+StmB9/eaDaSkGUO16eZL/vKQR9ZyS6deJ00zRmrbTuDD8LAHT194OpUVlKsAVIsQdQR3yAuJD0qTg65AjfiTsm/iuU+cYtQjfr8/OagSTss9nI2dUDlkJAAbVlH4t5AHO575YbSfO4a37th4AmQthauzfYpu3tl/7Sv290jp0KQZxmqZ3QIp1Jsp7R/dtrLY43ElJ1IPibLc+chK6kX1A366HXnMZjemmIfRERF7gzNr94n6St/9w4kEAP2VGUAqp0uTqSLneeMsoMT0j0CswO4xiOBrfTT15THYfpXUH7REb8N0PvcS0w/SLDEEkMjkIwuL5W1zjsg6ag30OnmQ4sPtGjaaHo3QDG7fCO23gA35D1mWwGKWsB1bq4Olwdx435ecvxWNPDYo7iMM48LggfOLW9jN60W+1sRmw9GoPtFT5j/6+8ojiY/o7iuv2QW3tTqkHyZf+ryqNSSyqpGx7TLOnXuRNLg37AmRwHaYTWUxZQI2JAyEtXhVeREMMDLIkyMzRVaMczkMASk22mhmcZrgg7iLTW7Yp3Ex9Q2JE55rZeD0P2ZTJzIvx6sq/aI+IL321txHhF6yRWJBDLcEEG43gjcw7xJ1fHrUGZ0XPbV0OUk/aIsQb+F4tIo23sn7JxbKKiU9KjoVRuRALW87WHfaeRV6zM7lmLXdiCTfiZ6ThsQUdXXerBh5G8wvSLBCjiaqL8GfMn4KnbT+VhOnBLTRz5o07KyIF+ZnOd0JSXSdBETJwnOI/NwH6851QaTBB4XFPScPTco43FT8+Y7jNthOmgqYatSxAy1HplFZQcrm4IuP3Dp4eG6YN+EVDrFcUzJtHpHQHbqpg8Th2vnZrpysVCsSeGiiE6yZ3oPs2tWrlaaEpkbOx0VeQJ5nlL2qCrFTvVip8QbGc2bpfHwudiG7ia68yOwPiE1hMMOCT6FQwoMAhhgZVE2AcxqGdUg1MUYbj1usw+0Blcze1hdTMTt1LNfvk8iKY2V2eJe2oNoEtEzSJZElGkPpXsOo+HTGhlNNVFJl1zWDsqvfiLsq+QkkObWvoL2Hjv8AkJrKFNK2ASkzAB0qIddQSzAG3MGxj43UrBNelR4zj8I6LTdrZaisyWN9BluCOB1k3F7MrUEzVKZVdFBJU3PAaGDxDkmhRcWanWdGHLM6XH8Wf0l1tSocdjFoIf8ASplsxHIfG3yUd5751+mjnUVv/hUYTZNd1DrSdlb4SFuCOcfW2PiAP2NTyRj8hPTaaBQFUWVQAANwAFgBHGSeZ/wqsK/p5ThdjYhzZaL3Gmq5QPEtYTSbM6Gm4bEOPwJr6ufoPOa9ePiY+8WWWTCsKXSRsXLRKqgCoDaw+Z/Myq6X0MmJYjc6q48dx91v5yeh1gum2q4d7fuupP8ACR/2kx2gHR1u2JrCZjejTdsTX3lYcOefSRTMMDAU4YShNgKpgQ8fWkYHWK0PZNB0mT6QpvmppzP9IaWhglG0GMqZkc/65RuaCa8SxkKL2HzydtzYtSvhMMaYU2FQkMbHV949JVazfBcuHoD/AGU99T840W47Rq9aZ5HjdiYlNWpOdd6DP53S9vGC2Ttp8KzFEUs1gwcENYcAb6b+U9UJgMRhUqC1REYcnUN85T81qpIT8NO4sy2E6dof2lJ1P3CrD3sZcYfpThXNg5U8mRh72t7wdbonhGNxTKH7jMB6Eke0hP0KQG9Os4tr21VvdcsH+t/1B/2L+M0FPG0iufrEym5uXUCx1HGRMR0hwyb6qt+C7/03lEvQZ+NZP4CfrJNHoSg/aVnP4FVP6s03mC+m9TfwFjumgF+opkn7T6Dxyjf6iXmza2JxOAqvWUkoy1UY2UZRowReQW5vG4TYGGpm60wzDi/bN+YB0HkJp9k9rMjfC6MhHcRaByjxI3mXWzMdGX7YmyzTHdGaJVjm3qSD4jQzWx4rRKb2SkeGDShxW2qdM2ZrnkNYfZ+26VTQNY8jpHtCuLLWsJGtD1TYTMbV23lJSnvG8xJSoeMbNTSErtt0wVPhMkm2qqm4cyxrbZzoL77axPaaof8AG07KJ6YBPjGZBH1n1MHmkdl6R2QTZVf2NH/wp8pjC82Tm9Gif9lPlMbVkKdOnQmHLHW0PgRGLOxFUImY7gyf1C8KMyVaI6wpXSJbSMLZDaStmvZ18YCosSk1iD3xWMQ3qiliaq8OsY/xHMPZp21NtHLlpm195kPpM9sQxH7yI38uX/rKhql41sRRXWObXfvnA2OmkHniZ4BtF5iOklVxbQd4lSz31MZlnWmbsVKuC5oocxlouWYJxeIXnERLTGOzzaUHvhqJ/wBu3oSPpMVaa/Z5vhqXcHH87wPho9Gzok6AcIsg9IHtSA+049ACfyk6nKjpIdUHJSfU2+kaIsi52HiespKTvXsnxH9rGTWW0zvRaqQzpwsD9D9PSaZxHJkOssjSZUEisIjKIz/SJj1ik/YA9GP5ypvLjpAO0h7nHusp7RktCyexC0TNFtEmoWz/2Q==')";
    document.getElementsByClassName("fixed-top navbar")[0].style.backgroundImage = "url('https://media.discordapp.net/attachments/917823225130516480/1105595479611412520/image.png?width=480&height=49')";
    navdrawer.style.backgroundSize = '120%'





    let nav = document.getElementById("nav-drawer").getElementsByTagName("nav"); // All nav on the sidebar

// https://cdn.donmai.us/original/04/56/0456072ca46f3c232cca5d30eb51edbc.jpg


    const len = nav.length;
    for (let i = 0; i < len; i++) { // For each nav
        let childs = nav[i].children;
        const num_ul = childs.length;
        for (let j = 0; j < num_ul; j++) { // For each ul in that nav
            let ul_children = childs[j].children;
            const num_li = ul_children.length;
            for (let k = 0; k < num_li; k++) {
                // for every li find "My courses"
                if (ul_children[k].textContent.trim() === "My courses") { // FOUND "My courses", courses should follow
                    return getReplaceDataFromInnerPages(ul_children, k + 1, num_li);
                }
            }
        }
    }
})();

async function getReplaceDataFromInnerPages(ul_children, first_li_index, ul_length) {
    for (; first_li_index < ul_length; first_li_index++) {
        // for every li
        getReplaceDataFromPage(ul_children[first_li_index]);
    }

}

async function getReplaceDataFromPage(li, index) {
    const name_element = li.getElementsByClassName("media-body")[0];
    const prev_name = name_element.textContent.trim();
    let saved_name = await GM.getValue(prev_name);

    name_element.style.color = "white";


    if (!saved_name) {
        // load the course page
        let response = await fetch(li.firstElementChild.href) // fetch website
        let data = await response.text(); // get the actual HTML as text

        let parser = new DOMParser();
        let new_page = parser.parseFromString(data, "text/html");
        let title = new_page.getElementsByTagName("title")[0].text.replace("Course:", "").trim().split("-"); // Extract title from page

        // get correct name
        let index_correct_name;
        if (title.length < 3) index_correct_name = 0; else index_correct_name = 1;
        saved_name = title[index_correct_name];
        GM.setValue(prev_name, saved_name);
    }

    // replace on the website
    name_element.textContent = saved_name;
}
